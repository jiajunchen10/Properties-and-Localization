package resourcesBundlePractice;

import java.util.Locale;
import java.util.ResourceBundle;

public class Practice {

	public static void main(String[] args) {
		Locale us = new Locale("en", "US");
		Locale cn = new Locale("zh","CN");
		System.out.println("english greetings: ");
		print(us);
		System.out.println("\n chinese greetings <make sure to change the text file encoding mode to UTF-8>: ");
		print(cn);
	}
	public static void print(Locale locale) {
		ResourceBundle rb = ResourceBundle.getBundle("resourcesBundlePractice.Practice",locale);
		rb.keySet().stream().map(x->x+" = "+rb.getString(x)).forEach(System.out::println);
	}

}
