package resourcesBundlePractice;

import java.util.ListResourceBundle;
import java.util.Locale;
import java.util.ResourceBundle;

public class JavaBundle_zh_CN extends ListResourceBundle{

	public static void main(String[] args) {
		ResourceBundle rb = ResourceBundle.getBundle("resourcesBundlePractice.JavaBundle",Locale.CHINA);
		rb.keySet().stream().forEach(x->System.out.println(x+ " = " + rb.getString(x)));
	}

	@Override
	protected Object[][] getContents() {
		return new Object[][] {{"greeting","你好"},{"go home", "再见"}};
	}

}
