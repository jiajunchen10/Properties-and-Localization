package resourcesBundlePractice;

import java.util.ListResourceBundle;

public class JavaBundle extends ListResourceBundle{

	@Override
	protected Object[][] getContents() {
		return new Object [][] {{"greeting","greeting from default"},{"go home","go home from default"}};
	}
}
