package resourcesBundlePractice;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Main {
	// ResourceBundle.getBundle("BundleName"); method call without specifying locale
	// ResourceBundle.getBundle("BundleName",locale); method call with locale specified
	public static void main(String[] args) {
		try {
			// if changing the argument of getBundle to JavaBundle, it will cause an exception, because you need to be exact on the bundle location, including the file name, package name
			ResourceBundle one = ResourceBundle.getBundle("resourcesBundlePractice.JavaBundle");   // getting the default resource bundle if locale is not specified
			print(one);
			}catch(MissingResourceException e) {
				System.out.println("can't find resourcesBundlePractice.JavaBundle");
			}
			catch(Exception e) {
				System.out.println("e xception occured");
			}
		ResourceBundle two = ResourceBundle.getBundle("resourcesBundlePractice.JavaBundle", Locale.US);
		System.out.println("\n printing JavaBundle_US: ");
		print(two);
		ResourceBundle three = ResourceBundle.getBundle("resourcesBundlePractice.JavaBundle", Locale.CHINA);
		System.out.println("\n printing JavaBundle_CN: ");
		print(three);
		
		//trying to print JavaBundle_CN without giving locale
		//locale arguement is not required by getBundle method, however, you need to give the exact name of the bundle (JavaBudnle_zh_CN), if not, getBundle method will returns the default
		System.out.println("\ntrying to print JavaBundle_CN without giving locale: ");
		ResourceBundle four = ResourceBundle.getBundle("resourcesBundlePractice.JavaBundle_zh_CN");
		print(four);
		
		System.out.println("=============== Currency and Number Formatting Practice ===================");
		double price = 19_999_999.99;   // you can put _ between the numbers but not before or after the numbers and not next to the decimal.
		NumberFormat general = NumberFormat.getInstance();  // this use the system default format, in this case, locale us.
		NumberFormat money = NumberFormat.getCurrencyInstance(Locale.CHINA);
		NumberFormat percent = NumberFormat.getPercentInstance(Locale.CHINA);
		System.out.println("US way of printing 19999999.99:   " + general.format(19999999.99));
		System.out.println("Gernmany way of printing 19999999.99:    "+NumberFormat.getInstance(Locale.GERMAN).format(19999999.99));
		System.out.println("19999999.99 in USD:  "+money.getCurrencyInstance(Locale.US).format(19999999.99));
		System.out.println("19999999.99 in CNY:  "+money.getCurrencyInstance(Locale.CHINA).format(19999999.99));
		System.out.println("US 20%:  "+NumberFormat.getPercentInstance(Locale.US).format(20));
		System.out.println("Chinese 20%:   "+percent.format(20));
		}
	public static void print(ResourceBundle rb) {
		rb.keySet().stream().forEach(x->System.out.println("key: "+x+" , value: "+rb.getString(x)));
	}
}
