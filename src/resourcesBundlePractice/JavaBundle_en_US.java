package resourcesBundlePractice;

import java.util.ListResourceBundle;
import java.util.Locale;
import java.util.ResourceBundle;

public class JavaBundle_en_US extends ListResourceBundle {

	public static void main(String[] args) {
		ResourceBundle rb = ResourceBundle.getBundle("resourcesBundlePractice.JavaBundle",Locale.US);
		rb.keySet().stream().forEach(x->System.out.println(x+ " = " + rb.getString(x)));

	}

	@Override
	protected Object[][] getContents() {
		return new Object[][] {{"greeting","hello"},{"go home","bye-bye"}};
	}

}
